package keys

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/cmd/lump/cmd/aws"
)

func init() {
	aws.AWSCmd.AddCommand(AWSKeysCmd)
}

var AWSKeysCmd = generateAWSKeysCmd()

func generateAWSKeysCmd() *cobra.Command {
	var createAWSKeysCmd = &cobra.Command{
		Use:   "key",
		Short: "AWS Key commands",
		Long:  `AWS Key Commands`,
	}

	return createAWSKeysCmd
}
