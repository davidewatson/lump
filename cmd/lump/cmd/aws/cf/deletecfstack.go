package cf

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/pkg/aws"
)

func init() {
	AWSCFCmd.AddCommand(generateDeleteCFStackCmd())
}

func generateDeleteCFStackCmd() *cobra.Command {
	var deleteCFStackCmd = &cobra.Command{
		Use:   "delete [stackName]",
		Short: "Deletes a cloud formation (or more stacks)",
		Long:  `Removes an AWS Cloud Formation stack`,
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteAWSCFStack(args)
		},
	}

	return deleteCFStackCmd
}

func deleteAWSCFStack(args []string) {
	awsutil.DeleteCFStack(args[0])
	/*	for _, keyName := range args {
			err := awsutil.DeleteCFStack(keyName)
			if err != nil {
				fmt.Printf("Could not delete key -->%s<--, reason was %s\n", keyName, err)
			} else {
				fmt.Printf("Was able to delete key -->%s<--\n", keyName)
			}
		}
	*/
}
