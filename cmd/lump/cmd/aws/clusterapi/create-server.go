package clusterapi

import (
	"encoding/json"
	"fmt"
	yaml2 "github.com/ghodss/yaml"
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/pkg/util/clusterapi"
	"io/ioutil"
	"os"
)

func init() {
	AWSClusterAPICmd.AddCommand(generateCreateClusterCmd())
}

func generateCreateClusterCmd() *cobra.Command {
	var printJSON bool

	var getCreateClusterCmd = &cobra.Command{
		Use:   "create-cluster [clusterName] [filename]",
		Short: "Returns a cluster-api cluster resource for the cluster",
		Long:  `Returns a cluster-api cluster resource for the cluster, filename is optional`,
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			createCluster(args, printJSON)
		},
	}
	getCreateClusterCmd.Flags().BoolVar(&printJSON, "json", false, "Set to output in JSON instead of YAML")

	return getCreateClusterCmd
}

func createCluster(args []string, printJSON bool) {
	clusterName := args[0]
	//stack, parameters, err := awsutil.GetCFStack(args[0])
	//if err != nil {
	//	fmt.Printf("Error retrieving stack: %s", err)
	//	os.Exit(1)
	//}

	clusterObject := clusterapiutil.GenerateCluster(clusterapiutil.ClusterTemplateParameters{Name: clusterName})
	data, err := json.Marshal(clusterObject)
	if err != nil {
		fmt.Printf("Error creating master record: %s", err)
		os.Exit(1)
	}
	if !printJSON {
		data, _ = yaml2.JSONToYAML(data)
	}
	if len(args) > 1 {
		// Going to dump the json to a file
		ioutil.WriteFile(args[1], data, 0644)
		fmt.Printf("Wrote to file %s\n", args[1])
		return
	}
	fmt.Printf("%s\n", data)

}
