package clusterapi

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/cmd/lump/cmd/aws"
)

func init() {
	aws.AWSCmd.AddCommand(AWSClusterAPICmd)
}

var AWSClusterAPICmd = generateAWSClusterAPICmd()

func generateAWSClusterAPICmd() *cobra.Command {
	var createAWSClusterAPICmd = &cobra.Command{
		Use:   "cluster-api",
		Short: "AWS cluster-api commands",
		Long:  `AWS cluseter-api Commands`,
	}

	return createAWSClusterAPICmd
}
