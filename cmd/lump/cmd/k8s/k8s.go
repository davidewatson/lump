package k8s

import (
	"github.com/spf13/cobra"
	"gitlab.com/mvenezia/lump/cmd/lump/cmd"
)

func init() {
	cmd.RootCmd.AddCommand(K8sCmd)
}

var K8sCmd = generateK8sCmd()

func generateK8sCmd() *cobra.Command {
	var createK8sCmd = &cobra.Command{
		Use:   "k8s",
		Short: "K8S commands",
		Long:  `Kubernetes Commands`,
	}

	return createK8sCmd
}
