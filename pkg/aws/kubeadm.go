package awsutil

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/sirupsen/logrus"
	"gitlab.com/mvenezia/lump/pkg/util/kubeadmutil"
)

// Definition of non AWS-provided Parameters for determining Kubeadm Initial Configuration
type KubeadmInitialConfigNonAWSParameters struct {
	PodCIDR          string
	ServiceCIDR      string
	K8sVersion       string
	DNSDomain        string
	PrivateAddresses bool
	PrintJSON        bool
}

// Helper function - instead of figuring out the right stack resource, this thing will
func GenerateKubeadmInitialConfigHelper(resources []*cloudformation.StackResource, nonAWSParameters KubeadmInitialConfigNonAWSParameters) ([]byte, error) {
	for _, item := range resources {
		if *item.LogicalResourceId == "K8sMaster" {
			return GenerateKubeadmInitialConfig(item, nonAWSParameters)
		}
	}
	return nil, fmt.Errorf("could not find K8SMaster resource in stack")
}

// Will return back a Kubeadm Initial Configuration for the cluster.  Presumes a single master
// Most of the AWS specific work is to figure out the ip address from the AWS API
func GenerateKubeadmInitialConfig(resource *cloudformation.StackResource, nonAWSParameters KubeadmInitialConfigNonAWSParameters) ([]byte, error) {
	var ipAddress string

	// Grabbing the control plane instance(s) from aws
	masterInstance, err := GetInstanceDetails(*resource.PhysicalResourceId)
	if err != nil {
		return nil, err
	} else if len(masterInstance.Instances) < 1 {
		return nil, fmt.Errorf("no master instances available apparently, therefore cannot create configuration")
	}
	// Let's warn the user since we're not expecting more than one control plane node
	if len(masterInstance.Instances) > 1 {
		logrus.Infof("Found %d instances of control plane, expecting 1; will use first instance, %s", len(masterInstance.Instances), *masterInstance.Instances[0].InstanceId)
	}

	// Determining the IP Address to be used
	if nonAWSParameters.PrivateAddresses {
		for _, j := range masterInstance.Instances[0].NetworkInterfaces {
			if *j.PrivateIpAddress != "" {
				ipAddress = *j.PrivateIpAddress
				break
			}
		}
		if ipAddress == "" {
			// Could not find a valid ip address, going to error out
			return nil, fmt.Errorf("could not find a private ip address for master node, specifically instance %s", *masterInstance.Instances[0].InstanceId)
		}
	} else {
		if *masterInstance.Instances[0].PublicIpAddress == "" {
			return nil, fmt.Errorf("could not find a public ip address for master node, specifically instance %s", *masterInstance.Instances[0].InstanceId)
		}
		ipAddress = *masterInstance.Instances[0].PublicIpAddress
	}

	// Now that we know the ip address, let's pass things along and return back a configuration
	configuration := kubeadmutil.GenerateInitialConfiguration(kubeadmutil.InitialConfigurationParameters{
		PodCIDR:     nonAWSParameters.PodCIDR,
		IPAddress:   ipAddress,
		K8sVersion:  nonAWSParameters.K8sVersion,
		DNSDomain:   nonAWSParameters.DNSDomain,
		ServiceCIDR: nonAWSParameters.ServiceCIDR,
	})

	return json.Marshal(configuration)
}
